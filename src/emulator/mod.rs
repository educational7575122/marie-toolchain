pub const MAXIMAL_ADDRESS: usize = 0xFFF;

pub struct Flash {
    pub address: [u16; MAXIMAL_ADDRESS]
}

impl Flash {
    pub fn new() -> Flash {
        Flash {
            address: [0; MAXIMAL_ADDRESS]
        }
    }

    pub fn flash_on_cell(&mut self, address: usize, value: u16) {
        self.address[address] = value;
    }
}

pub struct MemoryAccessRegister {
    flash: Flash,
}

impl MemoryAccessRegister {
    pub fn new(flash: Flash) -> MemoryAccessRegister {
        MemoryAccessRegister {
            flash,
        }
    }
}

pub struct ArithmeticLogicUnit {

}

pub struct MarieMachine {
    memory_access_register: MemoryAccessRegister
}