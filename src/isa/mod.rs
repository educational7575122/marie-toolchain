use std::error;
use std::fmt;
use crate::isa::IsaError::InvalidInstruction;
use crate::isa::internal::{unpack_command, compute_command_byte};

mod internal;

#[derive(PartialEq, Debug)]
pub enum InstructionSet {
    JnS(u16),
    Load(u16),
    Store(u16),
    Add(u16),
    Subt(u16),
    Input,
    Output,
    Halt,
    Skipcond(u16),
    Jump(u16),
    Clear,
    AddI(u16),
    JumpI(u16),
    LoadI(u16),
    StoreI(u16),
}

#[derive(Debug, Clone, PartialEq)]
pub enum IsaError {
    InvalidInstruction,
    MissingArgument,
    TooManyArguments
}

impl fmt::Display for IsaError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            IsaError::InvalidInstruction => write!(f, "Invalid instruction..."),
            IsaError::MissingArgument => write!(f, "Missing argument..."),
            IsaError::TooManyArguments => write!(f, "Too many arguments for given instruction...")
        }
    }
}

impl error::Error for IsaError {

}

impl InstructionSet {

    pub fn parse_line(line: &str) -> Result<InstructionSet, IsaError> {
        let mut processed_line = line.trim().to_uppercase();
        let comment_index = line.find(';');

        if let Some(index) = comment_index {
            processed_line = processed_line[..index].to_string();
        }
        let cleaned_line = processed_line.trim();
        let splitted : Vec<&str> = cleaned_line.split(" ").collect();
        unpack_command(&splitted)
    }

    pub fn parse_byte(byte: u16) -> Result<InstructionSet, IsaError> {
        let opcode = (byte & 0xF000) >> 12;
        let address = byte & 0x0FFF;
        match opcode {
            0 => Ok(InstructionSet::JnS(address)),
            1 => Ok(InstructionSet::Load(address)),
            2 => Ok(InstructionSet::Store(address)),
            3 => Ok(InstructionSet::Add(address)),
            4 => Ok(InstructionSet::Subt(address)),
            5 => Ok(InstructionSet::Input),
            6 => Ok(InstructionSet::Output),
            7 => Ok(InstructionSet::Halt),
            8 => Ok(InstructionSet::Skipcond(address)),
            9 => Ok(InstructionSet::Jump(address)),
            10 => Ok(InstructionSet::Clear),
            11 => Ok(InstructionSet::AddI(address)),
            12 => Ok(InstructionSet::JumpI(address)),
            13 => Ok(InstructionSet::LoadI(address)),
            14 => Ok(InstructionSet::StoreI(address)),
            _ => Err(InvalidInstruction)
        }
    }

    pub fn to_string(&self) -> String {
        match &self {
            InstructionSet::JnS(value) => format!("JNS {}", value),
            InstructionSet::Load(value) => format!("LOAD {}", value),
            InstructionSet::Store(value) => format!("STORE {}", value),
            InstructionSet::Add(value) => format!("ADD {}", value),
            InstructionSet::Subt(value) => format!("SUBT {}", value),
            InstructionSet::Input => format!("INPUT"),
            InstructionSet::Output => format!("OUTPUT"),
            InstructionSet::Halt => format!("HALT"),
            InstructionSet::Skipcond(value) => format!("SKIPCOND {}", value),
            InstructionSet::Jump(value) => format!("JUMP {}", value),
            InstructionSet::Clear => format!("CLEAR"),
            InstructionSet::AddI(value) => format!("ADDI {}", value),
            InstructionSet::JumpI(value) => format!("JUMPI {}", value),
            InstructionSet::LoadI(value) => format!("LOADI {}", value),
            InstructionSet::StoreI(value) => format!("STOREI {}", value),
        }
    }

    pub fn to_byte(&self) -> u16 {
        match &self {
            InstructionSet::JnS(value) => compute_command_byte(0, *value),
            InstructionSet::Load(value) => compute_command_byte(1, *value),
            InstructionSet::Store(value) => compute_command_byte(2, *value),
            InstructionSet::Add(value) => compute_command_byte(3, *value),
            InstructionSet::Subt(value) => compute_command_byte(4, *value),
            InstructionSet::Input => compute_command_byte(5, 0),
            InstructionSet::Output => compute_command_byte(6, 0),
            InstructionSet::Halt => compute_command_byte(7, 0),
            InstructionSet::Skipcond(value) => compute_command_byte(8, *value),
            InstructionSet::Jump(value) => compute_command_byte(9, *value),
            InstructionSet::Clear => compute_command_byte(10, 0),
            InstructionSet::AddI(value) => compute_command_byte(11, *value),
            InstructionSet::JumpI(value) => compute_command_byte(12, *value),
            InstructionSet::LoadI(value) => compute_command_byte(13, *value),
            InstructionSet::StoreI(value) => compute_command_byte(14, *value),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::isa::IsaError::{InvalidInstruction, MissingArgument, TooManyArguments};
    use super::{InstructionSet, internal};

    #[test]
    fn test_line_with_comments() {
        assert_eq!(InstructionSet::parse_line("Halt; testing"), Ok(InstructionSet::Halt));
        assert_eq!(InstructionSet::parse_line("Halt  ;testing"), Ok(InstructionSet::Halt));
        assert_eq!(InstructionSet::parse_line("Add 243; testing"), Ok(InstructionSet::Add(243)));
        assert_eq!(InstructionSet::parse_line("Add 243 ;testing"), Ok(InstructionSet::Add(243)));
    }

    #[test]
    fn test_parse_line() {
        assert_eq!(InstructionSet::parse_line("Halt"), Ok(InstructionSet::Halt));
        assert_eq!(InstructionSet::parse_line("Clear"), Ok(InstructionSet::Clear));
        assert_eq!(InstructionSet::parse_line("Input"), Ok(InstructionSet::Input));
        assert_eq!(InstructionSet::parse_line("Output"), Ok(InstructionSet::Output));
        assert_eq!(InstructionSet::parse_line("Add 123"), Ok(InstructionSet::Add(123)));
        assert_eq!(InstructionSet::parse_line("Subt 123"), Ok(InstructionSet::Subt(123)));
        assert_eq!(InstructionSet::parse_line("Load 123"), Ok(InstructionSet::Load(123)));
        assert_eq!(InstructionSet::parse_line("Store 123"), Ok(InstructionSet::Store(123)));
        assert_eq!(InstructionSet::parse_line("JnS 123"), Ok(InstructionSet::JnS(123)));
        assert_eq!(InstructionSet::parse_line("Skipcond 123"), Ok(InstructionSet::Skipcond(123)));
        assert_eq!(InstructionSet::parse_line("Jump 123"), Ok(InstructionSet::Jump(123)));
        assert_eq!(InstructionSet::parse_line("AddI 123"), Ok(InstructionSet::AddI(123)));
        assert_eq!(InstructionSet::parse_line("JumpI 123"), Ok(InstructionSet::JumpI(123)));
        assert_eq!(InstructionSet::parse_line("StoreI 123"), Ok(InstructionSet::StoreI(123)));
        assert_eq!(InstructionSet::parse_line("LoadI 123"), Ok(InstructionSet::LoadI(123)));
    }

    #[test]
    fn test_error_handling() {
        assert_eq!(InstructionSet::parse_line("DummYCommand"), Err(InvalidInstruction));
        assert_eq!(InstructionSet::parse_line("Halt 123"), Err(TooManyArguments));
        assert_eq!(InstructionSet::parse_line("Add"), Err(MissingArgument));
        assert_eq!(InstructionSet::parse_line("Add 123 233"), Err(TooManyArguments));
    }

    #[test]
    fn test_error_format() {
        assert_eq!(format!("{}", InvalidInstruction), "Invalid instruction...");
        assert_eq!(format!("{}", MissingArgument), "Missing argument...");
        assert_eq!(format!("{}", TooManyArguments), "Too many arguments for given instruction...");
    }

    #[test]
    fn test_parse_byte() {
        assert_eq!(InstructionSet::parse_byte(0x0100u16), Ok(InstructionSet::JnS(0x100)));
        assert_eq!(InstructionSet::parse_byte(0x1100u16), Ok(InstructionSet::Load(0x100)));
        assert_eq!(InstructionSet::parse_byte(0x2100u16), Ok(InstructionSet::Store(0x100)));
        assert_eq!(InstructionSet::parse_byte(0x3100u16), Ok(InstructionSet::Add(0x100)));
        assert_eq!(InstructionSet::parse_byte(0x4100u16), Ok(InstructionSet::Subt(0x100)));
        assert_eq!(InstructionSet::parse_byte(0x5100u16), Ok(InstructionSet::Input));
        assert_eq!(InstructionSet::parse_byte(0x6100u16), Ok(InstructionSet::Output));
        assert_eq!(InstructionSet::parse_byte(0x7100u16), Ok(InstructionSet::Halt));
        assert_eq!(InstructionSet::parse_byte(0x8100u16), Ok(InstructionSet::Skipcond(0x100)));
        assert_eq!(InstructionSet::parse_byte(0x9100u16), Ok(InstructionSet::Jump(0x100)));
        assert_eq!(InstructionSet::parse_byte(0xA100u16), Ok(InstructionSet::Clear));
        assert_eq!(InstructionSet::parse_byte(0xB100u16), Ok(InstructionSet::AddI(0x100)));
        assert_eq!(InstructionSet::parse_byte(0xC100u16), Ok(InstructionSet::JumpI(0x100)));
        assert_eq!(InstructionSet::parse_byte(0xD100u16), Ok(InstructionSet::LoadI(0x100)));
        assert_eq!(InstructionSet::parse_byte(0xE100u16), Ok(InstructionSet::StoreI(0x100)));
        assert_eq!(InstructionSet::parse_byte(0xF100u16), Err(InvalidInstruction));
    }

    #[test]
    fn test_serialize_as_string() {
        assert_eq!(InstructionSet::JnS(0x100).to_string(), "JNS 256");
        assert_eq!(InstructionSet::Load(0x100).to_string(), "LOAD 256");
        assert_eq!(InstructionSet::Store(0x100).to_string(), "STORE 256");
        assert_eq!(InstructionSet::Add(0x100).to_string(), "ADD 256");
        assert_eq!(InstructionSet::Subt(0x100).to_string(), "SUBT 256");
        assert_eq!(InstructionSet::Input.to_string(), "INPUT");
        assert_eq!(InstructionSet::Output.to_string(), "OUTPUT");
        assert_eq!(InstructionSet::Halt.to_string(), "HALT");
        assert_eq!(InstructionSet::Skipcond(0x100).to_string(), "SKIPCOND 256");
        assert_eq!(InstructionSet::Jump(0x100).to_string(), "JUMP 256");
        assert_eq!(InstructionSet::Clear.to_string(), "CLEAR");
        assert_eq!(InstructionSet::AddI(0x100).to_string(), "ADDI 256");
        assert_eq!(InstructionSet::JumpI(0x100).to_string(), "JUMPI 256");
        assert_eq!(InstructionSet::LoadI(0x100).to_string(), "LOADI 256");
        assert_eq!(InstructionSet::StoreI(0x100).to_string(), "STOREI 256");
    }

    #[test]
    fn test_byte_representation() {
        assert_eq!(InstructionSet::JnS(0x100).to_byte(), 0x0100);
        assert_eq!(InstructionSet::Load(0x100).to_byte(), 0x1100);
        assert_eq!(InstructionSet::Store(0x100).to_byte(), 0x2100);
        assert_eq!(InstructionSet::Add(0x100).to_byte(), 0x3100);
        assert_eq!(InstructionSet::Subt(0x100).to_byte(), 0x4100);
        assert_eq!(InstructionSet::Input.to_byte(), 0x5000);
        assert_eq!(InstructionSet::Output.to_byte(), 0x6000);
        assert_eq!(InstructionSet::Halt.to_byte(), 0x7000);
        assert_eq!(InstructionSet::Skipcond(0x100).to_byte(), 0x8100);
        assert_eq!(InstructionSet::Jump(0x100).to_byte(), 0x9100);
        assert_eq!(InstructionSet::Clear.to_byte(), 0xA000);
        assert_eq!(InstructionSet::AddI(0x100).to_byte(), 0xB100);
        assert_eq!(InstructionSet::JumpI(0x100).to_byte(), 0xC100);
        assert_eq!(InstructionSet::LoadI(0x100).to_byte(), 0xD100);
        assert_eq!(InstructionSet::StoreI(0x100).to_byte(), 0xE100);
    }
}
