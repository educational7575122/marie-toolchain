use super::{InstructionSet, IsaError};

pub fn is_command(text: &str) -> bool {
    let command_list = [
        "JNS", "LOAD", "STORE", "ADD", "SUBT",
        "INPUT", "OUTPUT", "HALT", "SKIPCOND",
        "JUMP", "CLEAR", "ADDI", "JUMPI", "LOADI",
        "STOREI"];
    command_list.contains(&mut text.as_ref())
}

pub fn compute_command_byte(opcode: u16, address: u16) -> u16 {
    // TODO: handle invalid opcode and length
    opcode << 12 | address
}

pub fn get_arguments_count(text: &str) -> u32 {
    match text {
        "INPUT" => 0,
        "OUTPUT" => 0,
        "HALT" => 0,
        "CLEAR" => 0,
        "JNS" => 1,
        "LOAD" => 1,
        "STORE" => 1,
        "ADD" => 1,
        "SUBT" => 1,
        "SKIPCOND" => 1,
        "JUMP" => 1,
        "ADDI" => 1,
        "JUMPI" => 1,
        "LOADI" => 1,
        "STOREI" => 1,
        _ => 2
    }
}

pub fn unpack_command(input: &Vec<&str>) -> Result<InstructionSet, IsaError> {
    let valid_command = is_command(&input[0]);
    if input.len() - 1 < get_arguments_count(&input[0]) as usize && valid_command {
        return Err(IsaError::MissingArgument)
    }
    else if input.len() - 1 > get_arguments_count(&input[0]) as usize && valid_command {
        return Err(IsaError::TooManyArguments)
    }
    let address = if input.len() == 2 {
        Some(input[1].parse::<u16>().unwrap())
    }
    else {
        None
    };
    // TODO: Check address validity
    match input[0] {
        "HALT" => Ok(InstructionSet::Halt),
        "CLEAR" => Ok(InstructionSet::Clear),
        "INPUT" => Ok(InstructionSet::Input),
        "OUTPUT" => Ok(InstructionSet::Output),
        "JNS" => Ok(InstructionSet::JnS(address.unwrap())),
        "LOAD" => Ok(InstructionSet::Load(address.unwrap())),
        "STORE" => Ok(InstructionSet::Store(address.unwrap())),
        "ADD" => Ok(InstructionSet::Add(address.unwrap())),
        "SUBT" => Ok(InstructionSet::Subt(address.unwrap())),
        "SKIPCOND" => Ok(InstructionSet::Skipcond(address.unwrap())),
        "JUMP" => Ok(InstructionSet::Jump(address.unwrap())),
        "ADDI" => Ok(InstructionSet::AddI(address.unwrap())),
        "JUMPI" => Ok(InstructionSet::JumpI(address.unwrap())),
        "LOADI" => Ok(InstructionSet::LoadI(address.unwrap())),
        "STOREI" => Ok(InstructionSet::StoreI(address.unwrap())),
        _ => Err(IsaError::InvalidInstruction)
    }
}