use std::fs;
use marie_toolchain::emulator::{Flash, MemoryAccessRegister};
use marie_toolchain::isa::InstructionSet;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct EmulatorArgs {
    /// file name of the file to be executed
    input: String
}

fn remove_headers(buffer: &[u8]) -> &[u8] {
    let header = "--FEEDFACE START--\n";
    let footer = "\n--DEADBEEF END--\n";
    let has_header = buffer[..header.len()].as_ref() == header.as_bytes();
    let has_footer = buffer[buffer.len() - footer.len()..].as_ref() == footer.as_bytes();
    if !has_header || !has_footer {
        panic!("Invalid binary format...");
    }
    return buffer[header.len()..(buffer.len() - footer.len())].as_ref()
}

fn prepare_flash(flash: &[u8]) -> Flash {
    let mut flash = Flash::new();
    let mut cell = 0_usize;
    /*for it in (0..flash.len()).filter(|x| x % 2 == 0) {
        let value = ((flash[it] as u16) << 8) | flash[it + 1] as u16;
        flash.flash_on_cell(cell, value);
        cell += 1;
    }*/
    flash
}

fn main() {
    let args = EmulatorArgs::parse();
    let buffer = fs::read(&args.input).expect("Invalid input file");
    let cleaned_bytes = remove_headers(&buffer.as_slice());
    let flash = prepare_flash(cleaned_bytes);
    let mut mar = MemoryAccessRegister::new(flash);
    println!("marie");
}
