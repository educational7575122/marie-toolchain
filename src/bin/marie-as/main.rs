use std::fs::File;
use std::io::prelude::*;
use clap::Parser;
use std::fs::read_to_string;
use marie_toolchain::isa::InstructionSet;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct AsArgs {
    /// file name of the file to be assemblied
    input: String,

    /// output file name
    #[arg(short, long, default_value_t=("a.out".to_string()))]
    output: String
}

fn push_header(header: &str, output: &mut Vec<u8>) {
    for ch in header.chars() {
        output.push(ch as u8);
    }
}

fn push_instruction(instruction: InstructionSet, output: &mut Vec<u8>) {
    let binary_representation = instruction.to_byte();
    let big_byte = ((binary_representation & 0xFF00) >> 8) as u8;
    let low_byte = (binary_representation & 0x00FF) as u8;
    output.push(big_byte);
    output.push(low_byte);
}

fn main() {
    let args = AsArgs::parse();
    let mut result: Vec<u8> = Vec::new();
    push_header("--FEEDFACE START--\n", &mut result);
    for line in read_to_string(args.input).expect("Reading input file: failed").lines() {
        let instruction = InstructionSet::parse_line(line);
        if let Ok(instruction) = instruction {
            push_instruction(instruction, &mut result);
        }
        else {
            panic!("{:?}", instruction.err().unwrap())
        }
    }
    push_header("\n--DEADBEEF END--\n", &mut result);

    let mut file = File::create(args.output)
        .expect("Writing output file: file creation failed");
    file.write_all(result.as_slice())
        .expect("Writing output file: failed");
}
