use std::fs;
use std::fs::File;
use std::io::Write;
use clap::Parser;
use marie_toolchain::isa::InstructionSet;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct ObjdumpArgs {
    /// file name of the file to be dumped
    input: String,

    /// output file name
    #[arg(short, long)]
    output: Option<String>,

    /// print on stdout
    #[arg(long, default_value_t=false)]
    stdout: bool
}

fn remove_headers(buffer: &[u8]) -> &[u8] {
    let header = "--FEEDFACE START--\n";
    let footer = "\n--DEADBEEF END--\n";
    let has_header = buffer[..header.len()].as_ref() == header.as_bytes();
    let has_footer = buffer[buffer.len() - footer.len()..].as_ref() == footer.as_bytes();
    if !has_header || !has_footer {
        panic!("Invalid binary format...");
    }
    return buffer[header.len()..(buffer.len() - footer.len())].as_ref()
}

fn dump_ok(instruction: &InstructionSet, args: &ObjdumpArgs, output: Option<&mut Vec<u8>>) {
    let hi_byte = (instruction.to_byte() >> 8) as u8;
    let low_byte = (instruction.to_byte() & 0x00FF) as u8;
    if args.stdout {
        println!("{:#02x} {:#02x} | {}",
                 hi_byte,
                 low_byte,
                 instruction.to_string());
    }
    if args.output.is_some() && (&output).is_some() {
        let output = output.unwrap();
        let stringified = format!("{}\n", instruction.to_string());
        for char in stringified.as_bytes().iter() {
            output.push(*char);
        }
    }
}

fn dump_err(instruction: u16, args: &ObjdumpArgs) {
    let hi_byte = (instruction >> 8) as u8;
    let low_byte = (instruction & 0x00FF) as u8;
    if args.stdout && args.output.is_none() {
        println!("{:#02x} {:#02x} | Invalid command",
                 hi_byte,
                 low_byte)
    }
    else {
        panic!("Cannot write output file with invalid command!");
    }
}

fn main() {
    let args = ObjdumpArgs::parse();
    let buffer = fs::read(&args.input).expect("Invalid input file");
    let cleaned_bytes = remove_headers(&buffer.as_slice());
    let mut output: Vec<u8> = Vec::new();

    for it in (0..cleaned_bytes.len()).filter(|x| x % 2 == 0) {
        let value = ((cleaned_bytes[it] as u16) << 8) | cleaned_bytes[it + 1] as u16;
        let instruction = InstructionSet::parse_byte(value);
        if let Ok(instruction) = instruction {
            dump_ok(&instruction, &args, Some(&mut output));
        }
        else {
            dump_err(value, &args);
        }
    }
    if args.output.is_some() {
        let mut file = File::create(args.output.unwrap())
            .expect("Writing output file: file creation failed");
        file.write_all(output.as_slice())
            .expect("Writing output file: failed");
    }
}
